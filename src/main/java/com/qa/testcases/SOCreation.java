package com.qa.testcases;
import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.qa.excelReader.ExcelReader;
import com.qa.excelReader.ExcelReaderByMap;
import com.qa.excelReader.ExcelWriterByColumnName;
import com.qa.pages.CashSale;
import com.qa.pages.CreditMemoPage;
import com.qa.pages.CustomerPage;
import com.qa.pages.CustomerRefundPage;
import com.qa.pages.ItemFulfilment;
import com.qa.pages.ItemReceiptPage;
import com.qa.pages.LoginPage;
import com.qa.pages.OpportunityPage;
import com.qa.pages.PurchaseOrderPage;
import com.qa.pages.QuotePage;
import com.qa.pages.ReturnAuthorizationPage;
import com.qa.pages.SalesOrderPage;
import com.qa.util.TestUtil;
import com.sun.mail.imap.protocol.Item;

import net.bytebuddy.build.Plugin.Factory.UsingReflection.Priority;

public class SOCreation extends TestUtil {

	
	String path="C:\\Users\\Sindhuja\\Desktop\\AllItems.xlsx";
	ExtentTest test;
	ExtentReports extent;
	ExtentSparkReporter htmlReporter;
	ExcelReader reader;
	LoginPage loginPage;
	CustomerPage customerPage;
	OpportunityPage opprPage;
	QuotePage quotePage;
	ItemFulfilment itemfulfilmentPage;
	ReturnAuthorizationPage returnPage;
	ItemReceiptPage itemReceiptPage;
	CreditMemoPage creditMemoPage;
	CustomerRefundPage customerRefundPage;
	CashSale cashSale;
	PurchaseOrderPage poPage;
	SalesOrderPage soPage;
	TestUtil testBase;
	public void send_email() throws EmailException {
		EmailAttachment attachment = new EmailAttachment();
		attachment.setPath("./GrowthProjectReport/GrowthProjectReport.html");
		attachment.setDisposition(EmailAttachment.ATTACHMENT);
		MultiPartEmail email = new MultiPartEmail();
		email.setHostName("smtp.gmail.com");
		email.setSmtpPort(465);
		email.setAuthenticator(new DefaultAuthenticator("sindhuja.b@tvarana.com", "Sindhu@123"));
		email.setSSLOnConnect(true);
		email.addTo("sindhuja.b@tvarana.com", "Sindhuja");
		email.setFrom("sindhuja.b@tvarana.com", "Sindhuja");
		email.setSubject("Growth Project Test Report");
		email.setMsg("Here is the report please find the attachment");
		email.attach(attachment);
		email.send();
	}

	@BeforeTest
	public void setExtent() {
		// specify location of the report
		htmlReporter = new ExtentSparkReporter(
				System.getProperty("user.dir") + "/GrowthProjectReport/GrowthProjectReport.html");
		htmlReporter.config().setDocumentTitle("Growth Project Test Report"); // Tile of report
		htmlReporter.config().setReportName("Growth Project Test Report"); // Name of the report
		htmlReporter.config().setTheme(Theme.STANDARD);
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// Passing General information
		extent.setSystemInfo("Environemnt", "QA");
		extent.setSystemInfo("user", "Sindhuja");
	}
	
	@AfterTest
	public void endReport() throws EmailException {
		extent.flush();
		send_email();
	}


	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {

		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report
		} else if (result.getStatus() == ITestResult.SKIP) {
			extent.removeTest(test);

		} else if (result.getStatus() == ITestResult.SUCCESS) {
			// test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}
		// driver.quit();
	}

	@DataProvider
	public Object[][] UKFD_RetailSalesOrder() throws IOException {
		reader = new ExcelReader();
		return reader.readExcelData("C:\\Users\\Sindhuja\\Desktop\\SanityTestCases.xlsx", 0);
	}
	
	
	@BeforeClass
	public void setUp() throws InterruptedException {
		testBase=new TestUtil();
		testBase.setUp();
	}
	
	@Test()
	public void TradeOrderCarpetVinyl() throws Exception 
	{
		String Terms="";
		test=extent.createTest("Verifying Trade Order via Contact Centre - Carpet/Vinyl Credit Card");
		customerPage=new CustomerPage();
		opprPage=new OpportunityPage();
		quotePage=new QuotePage();
		soPage=new SalesOrderPage();
		itemfulfilmentPage=new ItemFulfilment();
		itemReceiptPage =new ItemReceiptPage();
		poPage=new PurchaseOrderPage();
		ExtentTest test1 = null;
		test = extent.createTest("Verifying Trade Order via Contact Centre - Carpet/Vinyl Credit Card");
		ExcelReaderByMap reader2 = new ExcelReaderByMap();
		List<Map<String, String>> testData = reader2.getData("C:\\Users\\Sindhuja\\Desktop\\Allitems.xlsx","Allitems".trim());
		for (Map<String,String> data: testData) {
			//String Customerfname = data.get("Customer_Firstname");
			//String Customerlname=data.get("Customer_Lastname");
			String customername=name_get();
			String itemname=data.get("Item_Name");
			String quantity=data.get("Quantity");
			String phone=data.get("Phone");
			String Address1=data.get("Address1");
			String Address2=data.get("Address2");
			String Address3=data.get("Address3");
			String City=data.get("City");
			String State=data.get("State");
			String Zip=data.get("Zip");
			String Lead_source=data.get("Lead_source");
			String Location=data.get("Location");
			String Shipping_Method=data.get("Shipping_Method");
			String Delivery_Instructions=data.get("Delivery_Instructions");
			String Payment_Method=data.get("Payment_Method");
			String Credit_Card_Number=data.get("Credit_Card_Number");
			String Expiry_Date=data.get("Expiry_Date");
			String Security_Code=data.get("Security_Code");
			String NameOnCard=data.get("NameOnCard");
			String Sales_Order_Form=data.get("Sales_Order_Form");
			String Customer_type=data.get("Customer_type");
			String Bin=data.get("Bin");
			String lastfourdigits=data.get("lastfourdigits");
			customerPage.enter_Customer_details(customername, customername, customername+"@ukflooringdirect.co.uk", phone, Address1, Address2, Address3, City, State, Zip, Customer_type, "Sales Manager", Credit_Card_Number, NameOnCard, Expiry_Date, Security_Code, Payment_Method,Lead_source, test);
			customerPage.clickNewSOFromCustomer();
			soPage.enterSoDetails(Sales_Order_Form, Delivery_Instructions, Shipping_Method, customername, itemname, quantity, Payment_Method.trim(),Terms,lastfourdigits, test);
			 int rowNumber = reader2.getRowNumber(path, "soNumbers");
				if(itemname.startsWith("S"))
				{
					 String so_url=driver.getCurrentUrl();
					soPage.verifyEmail("Thanks for your sample order!", test);
					reader2.setCellData(path, "soNumbers", rowNumber, "Links", so_url);
					soPage.verifySOStatus("PENDING FULFILLMENT", test,rowNumber,"soNumbers");


				}
				else
				{
				String so_url=driver.getCurrentUrl();
				soPage.verifyEmail("Thanks for your order!", test);
				reader2.setCellData(path, "soNumbers", rowNumber, "Links", so_url);
			  	 soPage.salesOrderApproval(test);
				 soPage.verifyProcessedScreen(test);
				soPage.verifyCashSaleandPO("Purchase Order", test); 
				soPage.verifyEmail("Order Confirmation", test); 
				String sales_order_url=driver.getCurrentUrl();
				soPage.verifySOStatus("PENDING FULFILLMENT", test,rowNumber,"soNumbers");
		
				}
			}


			
			
			
	}
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	