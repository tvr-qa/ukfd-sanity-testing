package com.qa.excelReader;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriterByColumnName {
	public void excelReader(String filename,String sheetname,String columname,int rowNum,String value) throws IOException
	{
	FileInputStream file=new FileInputStream(filename);
	FileOutputStream fos=null;
	XSSFWorkbook book=new XSSFWorkbook(file);
	XSSFSheet sheet=book.getSheet(sheetname);
	XSSFRow row=null;
	XSSFCell cell=null;
	int colNum=-1;
	row=sheet.getRow(0);
	for(int i=0;i<row.getLastCellNum();i++)
	{
		if(row.getCell(i).getStringCellValue().trim().equals(columname))
		{
			colNum=i;
		}
	}
	row=sheet.getRow(rowNum);
	if(row ==null)
	row=sheet.createRow(rowNum);
	cell=row.getCell(colNum);
	if(cell==null)
	cell=row.createCell(colNum);
	cell.setCellValue(value);
	fos=new FileOutputStream(filename);
	book.write(fos);
	fos.close();
		

}
}
